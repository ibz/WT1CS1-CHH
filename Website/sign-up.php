<?php
session_start();
require_once('class.user.php');
$user = new USER();

if($user->is_loggedin()!=""){
	$user->redirect('suche.php');
}
if(isset($_POST['btn-signup'])){
	$uname = strip_tags($_POST['txt_uname']);
	$upass = strip_tags($_POST['txt_upass']);
		if($uname=="")	{
				$error[] = "provide username !";
		} else if($upass=="")	{
				$error[] = "provide password !";
		} else if(strlen($upass) < 6){
				$error[] = "Password must be atleast 6 characters";
		} else {
		try {
				$stmt = $user->runQuery("SELECT benutzer_name FROM benutzer WHERE benutzer_name=:uname");
				$stmt->execute(array(':uname'=>$uname));
				$row=$stmt->fetch(PDO::FETCH_ASSOC);

				if($row['user_name']==$uname) {
					$error[] = "sorry username already taken !";
				} else {
						if($user->register($uname,$upass)){
							$user->redirect('index.php?joined');

						}
				}
		} catch(PDOException $e) {
			echo $e->getMessage();
		}
	}
}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Coding Cage : Sign up</title>
		<link rel="stylesheet" href="style.css" type="text/css"  />
	</head>
	<body>
		<div class="signin-form">
			<div class="container">
        <form method="post" class="form-signin">
					<div class="logo">
 						<a href="."><img id="logo" src="images/logo.png" alt="Business Streamline" /></a>
 				 	</div>
            <h2 class="form-signin-heading">Sign up.</h2>
						<hr />
					<div id="error">
            <?php
						if(isset($error)){
			 				foreach($error as $error){
					 	?>
            <div class="alert alert-danger">
              <i class="form-signin-line"></i> &nbsp;
							<?php echo $error; ?>
            </div>
            <?php
						}
						} else if(isset($_GET['joined'])) {
				 		?>
	            <div class="alert alert-info">
	              <i class="form-signin-line">
								</i> &nbsp; Successfully registered
								<a href='index.php'>login</a> here
	            </div>
	            <?php
							}
							?>
					</div>
					<br />
          <div class="form-group">
          	<input type="text" class="form-control" name="txt_uname" placeholder="Enter Username" value="<?php if(isset($error)){echo $uname;}?>" />
          </div>
          <div class="form-group">
          	<input type="password" class="form-control" name="txt_upass" placeholder="Enter Password" />
          	<button type="submit" class="btn btn-primary" name="btn-signup">
        		<i class="btn-big"></i>SIGN UP
          	</button>
        	</div>
					<hr />
          <br />
          <label class="form-signin-line">You already have an account ?
						<a href="index.php">Sign In</a>
					</label>
        </form>
      </div>
		</div>
	</body>
</html>
