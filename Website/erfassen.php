<?php

	require_once("session.php");
  include 'config.php';
	require_once("class.user.php");
	$auth_user = new USER();

	$user_id = $_SESSION['user_session'];

	$stmt = $auth_user->runQuery("SELECT * FROM benutzer WHERE benutzer_id=:user_id");
	$stmt->execute(array(":user_id"=>$user_id));

	$userRow=$stmt->fetch(PDO::FETCH_ASSOC);

	// Einlesen der Formular-Felder
	if (isset($_POST['submit'])) {
	    $typ = $_POST['typ'];
	    $quali = $_POST['quali'];
			$menge = $_POST['menge'];
	    $lieferdatum = $_POST['lieferdatum'];
			$beschrieb = $_POST['beschrieb'];

	    //abfangen von falschen oder zu wehnig informationen.
					if($typ=="")	{
							$error[] = "du musst einen Typ für dein Teil angeben!";
					} else if($menge=="")	{
							$error[] = "du musst dem Anbieter sagen wieviele Teile du benötigst!";
					} else if(strlen($beschrieb) < 10){
							$error[] = "du solltest einen Detailierteren beschrieb verfassen...";
					} else {
					try{
							$stmt = $db->prepare("INSERT INTO nachfrager (benutzer_id, nachfr_typ,nachfr_quali, nachfr_menge, nachfr_lieferdatum, nachfr_beschr) VALUES (:benutzer_id, :nachfr_typ, :nachfr_quali, :nachfr_menge, :nachfr_lieferdatum, :nachfr_beschr)");
							$stmt->bindValue(':benutzer_id',$user_id);
							$stmt->bindValue(':nachfr_typ',$typ);
							$stmt->bindValue(':nachfr_quali',$quali);
							$stmt->bindValue(':nachfr_menge',$menge);
							$stmt->bindValue(':nachfr_lieferdatum',$lieferdatum);
							$stmt->bindValue(':nachfr_beschr',$beschrieb);
							$stmt->execute();
							/*** close the database connection ***/
							$db = null;
						} catch(PDOException $e) {
							echo $e->getMessage();
						}
						$error[] = "Deine $typ wurde in die Datenbank aufgenommen.";
					}
}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="style.css" type="text/css"  />
		<title>Hallo <?php print($userRow['benutzer_name']); ?></title>
	</head>
	<body>
 		<?php include 'nav.php';?>
		<div class="titel">
			<h1>erfassen</h1>
			<p>Suchen Sie ein Teil? Dann erfassen Sie es hier!</p>
		</div>
		<div class="inhalt">
			<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
				<table class="table table-unvsible"
					<tr>
						<td>Typ:</td>
						<td><input type="text" name="typ" value="" placeholder="zum beispiel: Schraube "/></td>
					</tr>
					<tr>
						<td>Qualität:</td>
						<td><input id="qualiInId" type="range" min="0" max="10" value="5" step="1" name="quali" oninput="qualiOutId.value = qualiInId.value"> &nbsp;
						<output name="qualiOut" id="qualiOutId">5</output></td>
					</tr>
					<tr>
						<td>Benötigte Stückzahl:</td>
						<td><input type="number" name="menge" min="1"  value="" placeholder="mindestens 1" /></td>
					</tr>
					<tr>
						<td>Lieferdatum:</td>
						<td><input type="date" name="lieferdatum" placeholder="YYYY-MM-DD"/>
						</td>
					</tr>
					<tr>
						<td>Beschrieb:</td>
						<td></td>
					</tr>
				</table>
				<div id="textbox">
					<textarea rows="10" cols="60" name="beschrieb">
					</textarea>
				</div>
				<span>
					<input class="btn" type="submit" name="submit" value="Nachfrage erstellen" />
				</span>
				<span id="error">
					<?php
						if(isset($error)){
							foreach($error as $error){
								echo $error;
							}
						}
					?>
				</span>
			</form>
		</div>
	</body>
</html>
