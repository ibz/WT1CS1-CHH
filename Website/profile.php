<?php
	require_once("session.php");
  require_once("config.php");
	require_once("class.user.php");
	$auth_user = new USER();

	$user_id = $_SESSION['user_session'];

	$stmt = $auth_user->runQuery("SELECT * FROM benutzer WHERE benutzer_id=:user_id");
	$stmt->execute(array(":user_id"=>$user_id));

	$userRow=$stmt->fetch(PDO::FETCH_ASSOC);
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="style.css" type="text/css"  />
		<title>Hallo <?php print($userRow['benutzer_name']); ?></title>
	</head>
	<body>
 		<?php include 'nav.php';?>
			<h1>Benutzerprofil</h1>
			<div class="inhalt">
				<table class="table">
					<caption>Deine Nachfragen:</caption>
					<th>Eintrag</th>
					<th>Typ</th>
					<th>Menge</th>
					<th>Qualität</th>
					<th>Lieferung</th>
					<th>Angebot</th>
					<th colspan="2">Alle Angebote vom Günstigsten zum Teuersten</th>
					<?php try {
							foreach ($db->query("
								SELECT * FROM nachfrager
								LEFT JOIN anbieter
								ON nachfrager.erfassungs_id = anbieter.erfassungs_id
								WHERE nachfrager.benutzer_id = $user_id
								ORDER BY nachfrager.nachfr_typ ASC, anbieter.angebot_preis ASC
								") as $row) {
							  echo
							  "<tr>" .
							    "<td>" . substr(htmlentities($row['nachfr_datum']),0,10) . "&nbsp;" . "</td>" .
							    "<td>" . htmlentities($row['nachfr_typ']) . "&nbsp;" . "</td>" .
							    "<td>" . htmlentities($row['nachfr_menge']) . "&nbsp;" . "</td>" .
							    "<td>" . htmlentities($row['nachfr_quali']) . "&nbsp;" . "</td>" .
							    "<td>" . htmlentities($row['nachfr_lieferdatum']) . "&nbsp;" . "</td>" .
									"<td>" . substr(htmlentities($row['angebot_datum']),0,10) . "&nbsp;" . "</td>" .
							    "<td>";
									if (!empty(htmlentities($row['angebot_preis']))) {
										echo
											"Fr. " . htmlentities($row['angebot_preis']) . "&nbsp;" . "</td>";
										} else {
											echo
											"kein Angebot bisher." . "&nbsp;" . "</td>";
										}
									if (!empty(htmlentities($row['angebot_file']))) {
										echo
										"<td>" .
											"dieses Angebot wurde von dir Angenommen" .
										"</td>";
									} else {
										if (!empty(htmlentities($row['angebot_preis']))){
											echo
											"<td>" .
												"<form method='GET' action='". "xml2.php" ."'>" .
													"<input type='hidden' name='angebot_id' value='".htmlentities($row['angebot_id'])."'/>" .
													"<input class='sbtn' type='submit' name='annehmen' value='Angebot annehmen' />" .
												"</form>" .
											"</td>";
										}
									}
							"</tr>";
						}

					} catch (Exception $e) {

						echo $e->getMessage();
						echo $e;
						}
					?>
			</table>
			<br />
			<table class="table">
				<caption>Deine Angebote:</caption>
				<th>Eintrag</th>
				<th>Typ</th>
				<th>Menge</th>
				<th>Qualität</th>
				<th>Lieferung</th>
				<th>A.Datum</th>
				<th>Angebot</th>
				<th colspan="2">Aufträge:</th>
				<?php try {
						foreach ($db->query("
							SELECT * FROM nachfrager
							LEFT JOIN anbieter
							ON nachfrager.erfassungs_id = anbieter.erfassungs_id
							WHERE anbieter.benutzer_id = $user_id
							ORDER BY nachfrager.nachfr_typ ASC, anbieter.angebot_preis ASC
							") as $row) {
							echo
							"<tr>" .
								"<td>" . substr(htmlentities($row['nachfr_datum']),0,10) . "&nbsp;" . "</td>" .
								"<td>" . htmlentities($row['nachfr_typ']) . "&nbsp;" . "</td>" .
								"<td>" . htmlentities($row['nachfr_menge']) . "&nbsp;" . "</td>" .
								"<td>" . htmlentities($row['nachfr_quali']) . "&nbsp;" . "</td>" .
								"<td>" . htmlentities($row['nachfr_lieferdatum']) . "&nbsp;" . "</td>" .
								"<td>" . substr(htmlentities($row['angebot_datum']),0,10) . "&nbsp;" . "</td>" .
								"<td>" . htmlentities($row['angebot_preis']) . "&nbsp;" . "</td>";
								if (!empty(htmlentities($row['angebot_file']))) {
									echo
									"<td>" .
										"<form method='GET' action='". "download.php" ."'>" .
											"<input type='hidden' name='angebot_id' value='".htmlentities($row['angebot_id'])."'/>" .
											"<input class='sbtn' type='submit' name='download' value='Download' />" .
										"</form>" .
									"</td>";
								} else {
									echo
									"<td>" .
										"dein Angebot wurde noch nicht bestätigt" .
									"</td>";
								}
						"</tr>";
					}

				} catch (Exception $e) {

					echo $e->getMessage();
					echo $e;
					}
				?>
		</table>
		<p>sobald ein Angebot angenommen wird wird dir ein Link mit den Informationen bereitgestellt.</p>
		<br />
		</div>
	</body>
</html>
