
<?php
class Database
{
    private $db_charset = "utf8";
    private $db_host = "localhost";
    private $db_name = "bsldb";
    private $db_username = "WT1CS1usr";
    private $db_password = "ibz4life-WT1CS1";
    public $conn;

    public function dbConnection()
	{

	    $this->conn = null;
        try
		{
            $this->conn = new PDO("mysql:host=" . $this->db_host . ";dbname=" . $this->db_name . ";charset" . $this->db_charset, $this->db_username, $this->db_password);
			$this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
		catch(PDOException $exception)
		{
            echo "Connection error: " . $exception->getMessage();
        }

        return $this->conn;
    }
}
?>
