<?php

	require_once("session.php");
  include 'config.php';
	require_once("class.user.php");
	$auth_user = new USER();

	$user_id = $_SESSION['user_session'];

	$stmt = $auth_user->runQuery("SELECT * FROM benutzer WHERE benutzer_id=:user_id");
	$stmt->execute(array(":user_id"=>$user_id));

	$userRow=$stmt->fetch(PDO::FETCH_ASSOC);

//collect Search
if (isset($_GET['keywords'])) {
	$keywords = $_GET['keywords'];
	$query = $db->query("
		SELECT *
		FROM nachfrager
		WHERE nachfr_typ LIKE '%{$keywords}%'
		OR nachfr_beschr LIKE '%{$keywords}%'
	");
} else {
	$query = $db->query("
		SELECT *
		FROM nachfrager
	");
}
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="style.css" type="text/css"  />
		<title>Hallo <?php print($userRow['benutzer_name']); ?></title>
	</head>
	<body>
 		<?php include 'nav.php';?>
			<div class="titel">
				<h1>Suchen</h1>
				<p>Suchen Sie ein Teil?
				</p>
				<p>grenzen Sie die Liste ein:</p>
				<form method="GET" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" >
				  <input type="search" name="keywords" autocomplete="off" placeholder="suche" onkeydown="searchq();"/>
				  <input type="submit"	value=">>"/>
				</form>
			</div>
			<div class="inhalt">
				<table class="table">
					<caption>Nachgefragte Teile:</caption>
					<th>Eintrag</th>
					<th>Typ</th>
					<th>Menge</th>
					<th>Qualität</th>
					<th>Lieferdatum</th>
					<?php
					if ($query->rowCount()) {
						while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
							echo
							"<tr>" .
								"<td>" . substr(htmlentities($row['nachfr_datum']),0,10) . "&nbsp;" . "</td>" .
								"<td>" . htmlentities($row['nachfr_typ']) . "&nbsp;" . "</td>" .
								"<td>" . htmlentities($row['nachfr_menge']) . "&nbsp;" . "</td>" .
								"<td>" . htmlentities($row['nachfr_quali']) . "&nbsp;" . "</td>" .
								"<td>" . htmlentities($row['nachfr_lieferdatum']) . "&nbsp;" . "</td>" .
								"<td>" .
									"<form method='GET' action='angebot.php'>" .
									  "<input class='sbtn' type='submit' value='Details' />" .
										"<input class='sbtn' type='hidden' name='id' value='".htmlentities($row['erfassungs_id'])."' />" .
									"</form>" .
 								"</td>" .
							"</tr>";
						}
					}
					?>
			</table>
		</div>
	</body>
</html>
