<?php

	require_once("session.php");
  include 'config.php';
	require_once("class.user.php");
	$auth_user = new USER();

	$user_id = $_SESSION['user_session'];

	$stmt = $auth_user->runQuery("SELECT * FROM benutzer WHERE benutzer_id=:user_id");
	$stmt->execute(array(":user_id"=>$user_id));

	$userRow=$stmt->fetch(PDO::FETCH_ASSOC);

	// Einlesen des Angebots-Feld:
	if (isset($_POST['submit'])) {
			$angebot = $_POST['submitname_angebot'];
			$erfassung_id = $_POST['submitname_erfassungs_id'];
	    //abfangen von falschen oder zu wehnig informationen.
	        if($angebot=="")	{
	            $error[] = "du musst einen Preis für dein Angebot angeben!";
	        } else if(strlen($angebot) > 9){
	            $error[] = "Dein Preis sollte maximal 7 Stellen und 2 Nachkomastellen haben.";
	        } else {
	        try{
	            $stmt = $db->prepare("
							INSERT INTO anbieter (benutzer_id, erfassungs_id, angebot_preis)
							VALUES (:benutzer_id, :erfassung_id, :angebot_preis)
							");
							$stmt->bindValue(':benutzer_id',$user_id);
							$stmt->bindValue(':erfassung_id',$erfassung_id);
	            $stmt->bindValue(':angebot_preis',$angebot);
	            $stmt->execute();
	            /*** close the database connection ***/
	            //$db = null;
	          } catch(PDOException $e) {
	            echo $e->getMessage();
	          }
	          $error[] = "Dein Angebot von Fr. $angebot wurde dem Nachfrager unterbreitet. Er wird das Angebot prüfen. Bei einer Annahme werden Sie eine E-Mail erhalten.";
	        }
	}

	//ID auslesen, welche vom Nachfrage Link mittels GET mitgegeben wurde
	if (isset($_GET['id'])) {
		$erfassung_id=$_GET['id'];
	}

?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="style.css" type="text/css"  />
		<title>Hallo <?php print($userRow['benutzer_name']); ?></title>
	</head>
	<body>
 		<?php include 'nav.php';?>
			<h1>Angebot abgeben:</h1>
				<p>Sie können zum gewählten Objekt nun ein Angebot abgeben.<br/>
				Bitte seien Sie sich bewusst dass der Nachfrager auf das Lieferdatum
				zählen wird.
				</p>
			<div class="inhalt">
				<table class="table">
	        <caption>Nachgefragte Teile:</caption>
	        <th>Eintrag</th>
	        <th>Typ</th>
	        <th>Menge</th>
	        <th>Qualität</th>
	        <th>Lieferdatum</th>
					<?php
					 foreach ($db->query("
					 SELECT * FROM nachfrager
					 WHERE erfassungs_id = $erfassung_id
					 ") as $row) {
					   echo
					   "<tr>" .
					     "<td>" . substr(htmlentities($row['nachfr_datum']),0,10) . "&nbsp;" . "</td>" .
					     "<td>" . htmlentities($row['nachfr_typ']) . "&nbsp;" . "</td>" .
					     "<td>" . htmlentities($row['nachfr_menge']) . "&nbsp;" . "</td>" .
					     "<td>" . htmlentities($row['nachfr_quali']) . "&nbsp;" . "</td>" .
					     "<td>" . htmlentities($row['nachfr_lieferdatum']) . "&nbsp;" . "</td>" .
					   "</tr>" .
					   "<tr>" .
					     "<td  colspan='5' class='titel'>" . "Beschreibung:" . "</td>" .
					   "</tr>" .
					   "<tr>" .
					     "<td colspan='5' class='beschreibung'>" . htmlentities($row['nachfr_beschr']) . "</td>" .
					   "</tr>";
					 }
					?>
	      </table>
				<div>
					<?php
		        if ($user_id!=(htmlentities($row['benutzer_id']))) {
		            echo
		            "<form method='post' action='".$_SERVER['PHP_SELF']."'>" .
		              "<h5>Angebot in Fr.:</h5>" .
		              "<div>" .
		                "<input id='submit_angebot' type='float' name='submitname_angebot' value='' placeholder='zum beispiel: 1000.00'/>" .
		                /*
		                *  ich übermittle hier versteckt die Erfassungs ID
		                *  damit die Zeile beim wiederaufruf auch angezeigt
		                *	 wird.
		                */
		                "<input id='submit_erfassungs_id' type='hidden' name='submitname_erfassungs_id' value='".$erfassung_id."'/>" .
		              "</div>" .
		              "<div>" .
		                "<input class='btn' type='submit' name='submit' value='Angebot abgeben' />" .
		              "</div>" .
		            "</form>";
		          }
		          else {
		            echo
		            "<div> Diese Nachfrage ist von dir erstellt worden. </div>";
		          }
		      ?>
				</div>
				<div id='error'>
				  <?php
					  if(isset($error)){
							foreach($error as $error){
								echo $error;
							}
						}
					?>
				</div>
			</div>
	</body>
</html>
