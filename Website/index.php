<?php
session_start();
require_once("class.user.php");
$login = new USER();

if($login->is_loggedin()!=""){
	$login->redirect('suche.php');
}
if(isset($_POST['btn-login'])){
	$uname = strip_tags($_POST['txt_uname']);
	$upass = strip_tags($_POST['txt_password']);
		if($login->doLogin($uname,$upass)){
		$login->redirect('suche.php');
		} else {
			$error = "Wrong Details !";
		}
}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>BusinessStreamline : Login</title>
		<link rel="stylesheet" href="style.css" type="text/css"  />
	</head>
	<body>
		<div class="signin-form">
			<div class="container">
        <form class="form-signin" method="post" id="login-form">
				  <div class="logo">
					  <a href="."><img id="logo" src="images/logo.png" alt="Business Streamline" /></a>
				  </div>
	        	<h2 class="form-signin-heading">Log In to WebApp.</h2>
						<hr />
	        <div id="error">
						<?php
						if(isset($error)){
						?>
						<div class="alert alert-danger">
							<i class="form-signin-line"></i> &nbsp;
							<?php echo $error; ?> !
						</div>
						<?php
						} else if(isset($_GET['joined'])) {
						?>
						<div class="alert alert-info">
							<i class="form-signin-line">
							</i> &nbsp; Successfully registered, please login now:
						</div>
						<?php
						}
						?>
        	</div>
					<br />
	        <div class="form-group">
	        	<input type="text" class="form-control" name="txt_uname" placeholder="Username" required />
	        	<span id="check-e">
						</span>
	        </div>
	        <div class="form-group">
	        	<input type="password" class="form-control" name="txt_password" placeholder="Your Password" />
	        	<button type="submit" name="btn-login" class="btn btn-default">
	          	<i class="btn-big"></i>SIGN IN
	        	</button>
	        </div>
					<hr />
	      	<br />
	        <label class="form-signin-line">You don't have account yet ?
						<a href="sign-up.php">Sign Up</a>
					</label>
	      </form>
	    </div>
		</div>
	</body>
</html>
