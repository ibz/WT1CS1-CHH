<?php
require_once("config.php");
//Creates XML string and XML document using the DOM
$dom = new DomDocument('1.0', 'UTF-8');

if (isset($_GET['angebot_id'])) {
	$angebot_id = $_GET['angebot_id'];
	$query = $db->query("
    SELECT * FROM nachfrager
    RIGHT OUTER JOIN anbieter
    ON nachfrager.erfassungs_id=anbieter.erfassungs_id
    WHERE anbieter.angebot_id=$angebot_id
	");
} else {
	echo "ein problem ist aufgetreten.";
  break;
}
//filename and path
$path="xml/";
$file="angebot_" . $_GET['angebot_id'] . ".xml";

//add root == artikel
$artikel = $dom->appendChild($dom->createElement('artikel'));
while ($result_array = $query->fetch(PDO::FETCH_ASSOC)) {
  //loop through each key,value pair in row
  foreach($result_array as $key => $value) {
     //$key holds the table column name

    //add track element to jukebox
    $keys = $dom->createElement($key,$value);
    $artikel->appendChild($keys);

  }
}
$dom->formatOutput = true; // set the formatOutput attribute of domDocument to true

// save XML as string or file
$test1 = $dom->saveXML(); // put string in test1 for testings
$dom->save($path.$file); // save as file

// Filepath wird in Datenbank gespeichert
if($file=="")	{
    $error[] = "es ist was mit dem Filepath falsch gelaufen.";
} else {
try{
    $query = $db->query("
      UPDATE anbieter
      SET angebot_file='$file'
      WHERE angebot_id='$angebot_id'
      ");
    /*** close the database connection ***/
    //$db = null;
  } catch(PDOException $e) {
    echo $e->getMessage();
  }
  $error[] = "Ihr Angebot ist nun über den Link nun bereitgestellt";
}

// Redirect to this page.
   header("Location: " . "profile.php");
   exit();

?>
